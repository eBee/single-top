# Requirements

- Rivet 2.5.3 (which implies C++11, which is also used in the custom analyses)
- Sherpa 2.2.3 (the `single-top` branch can be used to enable special debugging output) using OpenLoops 1.3.1


# Considerations

- `MC_SINGLE_TOP_*` as it is should only be used with fragmentation being
  enabled, as it is required for the bTagged method of Rivet.
