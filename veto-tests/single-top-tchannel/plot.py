#!/usr/bin/env python2

import os
import sys
from string import Template
import matplotlib.pyplot as plt
import heppyplotlib as hpl
import heppyplotlib.configuration as hpc
import heppyplotlib.yodaplot as hpy
import heppyplotlib.rivetplot as hpr

channels = {}

analysis = "/MC_SINGLE_TOP/"

def cut_label_twojets():
    return "$N_\\text{jets} = 2$"

def cut_label_GT(symbol='G', yref='0', cut=50):
    return "$" + symbol + "_{T, \\text{cut}}^{(" + yref + ")} = " + str(cut) + "\\,\\text{GeV}$"

def cut_label_Dy(cut=50):
    return "$y_\\text{cut} = " + str(cut) + "$"

def cut_suffix_twojets():
    return "_twojets_cut"

def cut_suffix_GT(symbol='G', yref='0', cut=50):
    return "_{}T{}_cut_{}".format(symbol, yref, cut)

def cut_suffix_Dy(cut=1.5):
    return "_absy_cut_{}".format(cut)

channels["comparison"] = {
        "cuttext": "cuts",
        "cutsuffixes": [
            cut_suffix_twojets(),
            cut_suffix_GT(cut=30),
            cut_suffix_GT(yref='t', cut=30),
            cut_suffix_Dy(cut=2)
            ],
        "labels": [
            cut_label_twojets(),
            cut_label_GT(cut=30),
            cut_label_GT(yref='t', cut=30),
            cut_label_Dy(cut=2)
            ]
        }

unvaried_histograms = ["xs_before_centrality_cuts"]

for cname, cdata in channels.items():

    process = os.path.split(os.getcwd())[-1]
    texprocess = process
    diffylim = [0.5, 2.0]
    diffyticks = [0.5, 1.0, 1.5]
    if process == 'top_pair':
        process = "top-antitop production"
        texprocess = "$t\\bar{t}$ production"
        diffylim = [0.2, 5.0]
        diffyticks = [0.5, 1.0, 2.0, 4.0]
    elif process == 'single-top-tchannel':
        process = "single-top production (t-channel)"
        texprocess = "$tj$ production (t-channel)"
    elif process == 'w':
        process = "W-boson plus jets production"
        texprocess = "$Wjj$ production"

    plots_path = 'plots-' + cname
    try:
        os.mkdir(plots_path)
    except OSError:
        pass

    # why is this necessary? ~> bug in hpl
    hpc.use_tex()

    yodafile = 'Run/Analysis/Reweighting.yoda'
    if not os.path.exists(yodafile):
        yodafile = 'Run-LO/Analysis/Reweighting.yoda'

    rivet_paths = hpy.data_object_names(yodafile)
    rivet_paths = [path for path in rivet_paths
                   if cdata["cutsuffixes"][0] in path]
    rivet_paths += unvaried_histograms
    rivet_paths.sort(key=str.lower)
    print(rivet_paths)

    headers = {'b_': 'bottom tag jet',
               'j_': '$p_T$-ordered light jets',
               'jt': 'top + light jet system',
               'l_': 'lepton',
               't_': 'reconstructed top quark',
               'W_': 'reconstructed $W$ boson',
               'xs': 'total cross sections'}

    html = ""

    ordered_keys = sorted(headers.keys())
    for anchor in ordered_keys:
        title = headers[anchor]
        html += "<a href=\"#{}\">{}</a><br>".format(anchor, title)

    for rivet_path in rivet_paths:
        histogram = rivet_path.split('/')[-1]
        histogram = histogram.replace(cdata["cutsuffixes"][0], "")
        base_rivet_path = analysis + histogram
        plotinfo = hpr.load_plot_info(base_rivet_path)
        try:
            key = histogram[:2]
            header = headers[key]
            html += "<h2 id=\"{}\">{}</h2>\n".format(key, header)
            del headers[key]
        except KeyError:
            pass
        print("Processing {} ...".format(histogram))
        rivet_path_variants = []
        if rivet_path in unvaried_histograms:
            rivet_path_variants.append(base_rivet_path)
        else:
            for suffix in cdata["cutsuffixes"]:
                rivet_path_variants.append(base_rivet_path + suffix)
        data_objects = []
        for rivet_path_variant in rivet_path_variants:
            data_objects.append(hpy.resolve_data_object(yodafile, rivet_path_variant))
        axes_list = hpl.ratioplot(data_objects, base_rivet_path)[0]

        (main, diff) = axes_list


        #hpr.set_axis_scales(plotinfo, main, diff)

        #main.set_yscale('linear')
        if not 'RatioPlotYMin' in plotinfo:
            diff.set_ylim(diffylim)
            diff.set_yticks(diffyticks)
        diff.set_ylabel("ratio to $N_\\text{jets}=2$")
        #main.set_ylabel("$\\text{d}\\sigma / \\text{d}
        #main.set_ylabel("$(\\sigma_t - \\sigma_{\\bar{t}}) / (\\sigma_t + \\sigma_{\\bar{t}})$")

        histogram = histogram.replace('_', '-')
        histogram_title = histogram.replace('-', ' ')

        #plt.suptitle(histogram)

        (handles, labels) = main.get_legend_handles_labels()
        main.legend(handles, cdata["labels"])

        fig_filename = histogram + '.png'
        fig_path = '{}/{}'.format(plots_path, fig_filename)
        plt.savefig(fig_path)
        plt.close()

        html += "<img src='{}' alt='{}'>\n".format(fig_filename, histogram)

    with open('plot_index_template.html', 'r') as f:
        templatehtml = Template(f.read())
        with open(plots_path + '/index.html', 'w') as g:
            g.write(templatehtml.substitute(content=html, process=process, texprocess=texprocess, cuttext=cdata["cuttext"]))
