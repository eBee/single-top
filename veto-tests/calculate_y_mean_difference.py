#!/usr/bin/env python2
# coding: utf-8

from __future__ import print_function

import math

import heppyplotlib as hpl
import heppyplotlib.yodaplot as hpy

def yodafilepath(process):
    return process + '/Run/Analysis/Reweighting.yoda'

histogram = "j_y_1"

processes = ["single-top-tchannel", "single-antitop-tchannel"]

analyses = ['/MC_SINGLE_TOP_TWO_JETS_VETO/',
            '/MC_SINGLE_TOP_TOP_GT_VETO_15/',
            '/MC_SINGLE_TOP_TOP_GT_VETO_30/',
            '/MC_SINGLE_TOP_TOP_GT_VETO/',
            '/MC_SINGLE_TOP_CENTRAL_GT_VETO_15/',
            '/MC_SINGLE_TOP_CENTRAL_GT_VETO_30/',
            '/MC_SINGLE_TOP_CENTRAL_GT_VETO/',
            '/MC_SINGLE_TOP_RAPIDITY_DIFF_VETO_1_0/',
            '/MC_SINGLE_TOP_RAPIDITY_DIFF_VETO/',
            '/MC_SINGLE_TOP_RAPIDITY_DIFF_VETO_2_00/']

extreme_diffs = [1e20, 0]
for analysis in analyses:
    print(analysis)
    mean_rapidities = []
    for process in processes:
        filepath = yodafilepath(process)
        rivetpath = analysis + histogram
        data_object = hpy.resolve_data_object(filepath, rivetpath)

        normalisation_factor = 0.0
        mean_rapidity = 0.0
        for bin in data_object.bins:
            x = abs(bin.xMid)
            y = bin.sumW
            mean_rapidity += x * y
            normalisation_factor += y
        mean_rapidity /= normalisation_factor
        mean_rapidities.append(mean_rapidity)

        mean_rapidity_error = 0.0
        for bin in data_object.bins:
            x = abs(bin.xMid)
            # NOTE: this requires that no bin crosses x=0.0, which
            # is true for our j_y_1 histogram
            xerr = bin.xWidth * 0.68 / 2.0
            y = bin.sumW
            yerr = math.sqrt(bin.sumW2)
            mean_rapidity_error += (
                    (y * xerr)**2
                    + ((x - mean_rapidity_error) * yerr)**2
                    )
        mean_rapidity_error = math.sqrt(mean_rapidity_error) / normalisation_factor

        print(process + ":", mean_rapidity, u"±", mean_rapidity_error)

    diff = mean_rapidities[0] - mean_rapidities[1]
    print("Δ|y| =", diff)
    extreme_diffs[0] = min(extreme_diffs[0], diff)
    extreme_diffs[1] = min(extreme_diffs[1], diff)

print("Δ|y|_min =", extreme_diffs[0])
print("Δ|y|_max =", extreme_diffs[1])
