#ifndef RIVET_MC_SINGLE_TOP_TWOJETS_HH
#define RIVET_MC_SINGLE_TOP_TWOJETS_HH

#include "MC_SINGLE_TOP.hh"

namespace Rivet {

  class MC_SINGLE_TOP_TWOJETS : public MC_SINGLE_TOP {
  public:

    MC_SINGLE_TOP_TWOJETS();

  };

}

#endif

// vim: sw=2 et
