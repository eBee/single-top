#include "MC_SINGLE_TOP_ATLAS.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Tools/SmearingFunctions.hh"
#include "Rivet/AnalysisLoader.hh"

namespace Rivet {

  /// Minimal constructor
  MC_SINGLE_TOP_ATLAS::MC_SINGLE_TOP_ATLAS() : Analysis("MC_SINGLE_TOP_ATLAS")
  {
  }

  void MC_SINGLE_TOP_ATLAS::init()
  {
    // dressed electrons and/or muons (NOTE that we require tau leptons to be
    // either disabled in the event generation or properly decayed)
    const FinalState& photonfs{ Cuts::open() };
    const FinalState& leptonfs{ Cuts::abseta < 2.5 && Cuts::pT > 25*GeV };
    DressedLeptons leptons{ photonfs, leptonfs, 0.1 };
    addProjection(leptons, "DressedLeptons");

    VetoedFinalState jetfs{ FinalState{ Cuts::abseta < 4.5 && Cuts::pT > 0*GeV } };
    jetfs.addVetoOnThisFinalState(leptons);
    FastJets jets(jetfs, FastJets::ANTIKT, 0.4);
    addProjection(jets, "Jets");

    addProjection(MissingMomentum{}, "MissingET");

    addProjection(ChargedFinalState{ Cuts::abseta < 2.5 && Cuts::pT > 0.4*GeV }, "ChargedFinalState");

    _h_xs = bookHisto1D(1, 1, 1);
    _h_t_pt = bookHisto1D(2, 1, 1);
    _h_t_y = bookHisto1D(3, 1, 1);
    _h_j_pt = bookHisto1D(4, 1, 1);
    _h_j_y = bookHisto1D(5, 1, 1);

  }

  /// returns true if the GenParticle particle is from a hadronic decay
  /// excluding pure tau decays
  bool IsHadronicRemnant2(const HepMC::GenParticle* p) { 
    if (p->production_vertex()->id() != 6) {
      return false;
    }
    // either hadronic or tau decay, for taus we will need to recurse
    bool processed_first{ false };
    for (HepMC::GenVertex::particles_in_const_iterator q{ p->production_vertex()->particles_in_const_begin() };
        q != p->production_vertex()->particles_in_const_end();
        ++q) {
      assert(!processed_first); // hadronic vertex is expected to have only one in-particle
      if (std::abs((*q)->pdg_id()) == 15) {
        return IsHadronicRemnant2(*q);
      }
      processed_first = true;
    }
    return true;
  }

#define debug(literal) MSG_DEBUG(literal);

  void MC_SINGLE_TOP_ATLAS::analyze(const Event& event)
  {
    const MissingMomentum& met = apply<MissingMomentum>(event, "MissingET");
    if (met.missingPt() < 30*GeV)
      vetoEvent;

    // obtain photon-dressed electrons and muons
    const vector<DressedLepton>& dressedLeptons {
      apply<DressedLeptons>(event, "DressedLeptons").dressedLeptons() };
    debug(dressedLeptons.size() << " leptons dressed");
    if (dressedLeptons.size() == 0)
      vetoEvent;

    vector<DressedLepton> leptons;
    leptons.reserve(dressedLeptons.size());
    std::remove_copy_if(
        dressedLeptons.begin(), dressedLeptons.end(),
        std::back_inserter(leptons),
        [&](const DressedLepton& l) {
          return IsHadronicRemnant2(l.constituentLepton().genParticle());
        });
    debug(leptons.size() << " leptons survive implicit W-boson match");

    if (leptons.size() != 1)
      vetoEvent;
    const auto& lepton = leptons[0];

    // obtain jets
    const FastJets& alljets = apply<FastJets>(event, "Jets");
    const Jets& jets = alljets.jetsByPt(30*GeV);

    for (const auto& jet : jets) {
      if (deltaR(jet.momentum(), lepton.momentum()) < 0.4)
        vetoEvent;
    }

    const double weight = event.weight();

    // sort jets into b-tagged jets and light jets
    Jets bjets, ljets;
    foreach (const Jet& jet, jets) {
      // we assume that b-tagging efficiencies and mis-tagging acceptances are
      // actually taken care of for the results in arXiv:1702.02859; hence, we
      // can use perfect experimental b-tagging here
      if (jet.bTagged() && jet.momentum().abseta() < 2.5) {
        bjets.push_back(jet);
      } else {
        ljets.push_back(jet);
      }
    }

    // require at least one light jet; in arXiv:1702.02859, exactly one light
    // jet is required, we implement this with an MCSingleTopCut subclass
    if (ljets.size() != 1) {
      debug(ljets.size() << " light jets -> veto");
      vetoEvent;
    }

    // b-jet cuts
    if (bjets.size() != 1) {
      debug(ljets.size() << " bottom jets -> veto");
      vetoEvent;
    }
    const Jet& ljet = ljets[0];
    const Jet& bjet = bjets[0];
    if ((bjet.momentum() + lepton.momentum()).mass() > 160*GeV) {
      debug("too off-shell -> veto");
      vetoEvent;
    }

    debug("passed minimum cuts");

    // reconstruct neutrino momentum using W mass as constraint, cf.
    // arXiv:1502.05923
    const double a{ lepton.E2() - lepton.pz2() };
    static const double mW2{ 80.385*80.385 };
    const double k{ (mW2 - lepton.mass2()) / 2.0
      + ( lepton.px() * met.missingMomentum().px()
          + lepton.py() * met.missingMomentum().py() ) };
    const double b{ - 2.0 * k * lepton.pz() };
    const double c{ lepton.E2() * met.missingPt() * met.missingPt() - k * k};
    double neutrino_pz{ - b / (2.0 * a) };
    const double radicand{ b * b - 4.0 * a * c };
    if (radicand > 0.0) {
      const double reduced_sqrt{ sqrt(radicand) / (2.0 * a) };
      const double neutrino_pz1{ neutrino_pz + reduced_sqrt };
      const double neutrino_pz2{ neutrino_pz - reduced_sqrt };
      neutrino_pz = (std::abs(neutrino_pz1) < std::abs(neutrino_pz2)) ?
        neutrino_pz1 : neutrino_pz2;
    }
    FourMomentum neutrinomom;
    neutrinomom.setPM(met.missingMomentum().px(),
        met.missingMomentum().py(),
        neutrino_pz,
        0.0);

    // reconstruct W and top momentum
    const FourMomentum wmom{ lepton.momentum() + neutrinomom };
    const FourMomentum tmom{ wmom + bjet.momentum() };

    _h_xs->fill(0.5, weight);
    _h_t_pt->fill(tmom.pT(), weight);
    _h_t_y->fill(tmom.absrap(), weight);
    _h_j_pt->fill(ljet.momentum().pT(), weight);
    _h_j_y->fill(ljet.momentum().absrap(), weight);
  }


  void MC_SINGLE_TOP_ATLAS::finalize()
  {
    const double br{ 0.333328 };
    const double sf{ crossSection() / sumOfWeights() / br };
    scale(_h_xs, sf);
    scale(_h_t_pt, sf);
    scale(_h_t_y, sf);
    scale(_h_j_pt, sf);
    scale(_h_j_y, sf);
  }

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_SINGLE_TOP_ATLAS);

}

// vim: sw=2 et
