#ifndef RIVET_MC_SINGLE_TOP_INCL_HH
#define RIVET_MC_SINGLE_TOP_INCL_HH

#include "Rivet/Analysis.hh"

namespace Rivet {

  class MC_SINGLE_TOP_INCL : public Analysis {
  public:

    MC_SINGLE_TOP_INCL();

    /// @name Analysis methods
    //@{

    void init();
    void analyze(const Event&);
    void finalize();

    //@}

  private:

    size_t _njet{ 4 };
    size_t _maxjetmulti{ 30 };

    double avgx;
    double avgQ;
    double totalweight;

    // @name Histogram data members
    //@{

    Histo1DPtr _h_t_mass;
    Histo1DPtr _h_t_pt;
    Histo1DPtr _h_t_y;

    std::vector<Histo1DPtr> _h_j_mass;
    std::vector<Histo1DPtr> _h_j_pt;
    std::vector<Histo1DPtr> _h_j_y;

    std::vector<Histo1DPtr> _h_b_mass;
    std::vector<Histo1DPtr> _h_b_pt;
    std::vector<Histo1DPtr> _h_b_y;

    Histo1DPtr _h_j_multi_exclusive;
    Histo1DPtr _h_b_multi_exclusive;
    Histo1DPtr _h_j_multi_inclusive;

    Histo1DPtr _h_xs;

    Histo1DPtr _h_x;
    Histo1DPtr _h_q;

    //@}

  };

}

#endif

// vim: sw=2 et
