# BEGIN PLOT /MC_SINGLE_TOP/.*_y
XMin=-4
XMax=4
LogY=0
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/W_y
Title=$W$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/b_y
Title=$b$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/t_y
Title=$t$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/t_y_minus_j_y_1
Title=$\Delta y(t, j_1)$
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/t_y_minus_j_y_2
Title=$\Delta y(t, j_2)$
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/t_y_minus_j_y_3
Title=$\Delta y(t, j_3)$
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/t_y_minus_j_y_4
Title=$\Delta y(t, j_4)$
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/l_y
Title=$\ell$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_y_1
Title=$\mathrm{j}_1$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_y_2
Title=$j_2$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_y_3
Title=$j_3$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_y_4
Title=$j_4$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_y_1
Title=$(t + j_1)$ (top quark + tag jet) rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_y_2
Title=$(t + j_2)$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_y_3
Title=$(t + j_3)$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_y_4
Title=$(t + j_4)$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/.*_pt
LogY=0
XLabel=$p_T$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_T$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_pt_
LogX=1
LogY=1
XLabel=$p_T$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_T$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_pt_1
Title=$\mathrm{j}_1$ transverse momentum
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_pt_2
Title=$j_2$ transverse momentum
YMin=1e-12
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_pt_3
Title=$j_3$ transverse momentum
YMin=1e-15
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_pt_4
Title=$j_4$ transverse momentum
YMin=1e-15
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/t_pt
Title=$t$ transverse momentum
LogY=1
RatioPlotYMin=0.2
RatioPlotYMax=5.0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/W_pt
Title=$W$ transverse momentum
LogY=1
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/b_pt
Title=$b$ transverse momentum
LogY=1
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/l_pt
Title=$\ell$ transverse momentum
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/.*_mass
LogY=1
XLabel=$m$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}m$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_mass_1
Title=$j_1$ (tag jet) mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_mass_2
Title=$j_2$ mass
YMin=1e-12
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_mass_3
Title=$j_3$ mass
YMin=1e-15
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_mass_4
Title=$j_4$ mass
YMin=1e-18
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_mass_
LogX=1
XMin=50
XMax=1e3
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_mass_1
Title=$(t + j_1)$ (top quark + tag jet) mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_mass_2
Title=$(t + j_2)$ mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_mass_3
Title=$(t + j_3)$ mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/jt_mass_4
Title=$(t + j_4)$ mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/t_mass
Title=$t$ mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/W_mass
Title=$W$ mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/b_mass
Title=$b$ mass
XMin=0
XMax=40
YMin=1e-6
YMax=1e1
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/j_multi_exclusive
Title=exclusive light jet multiplicity
XMin=0
XMax=10
YMin=1e-12
YMax=1e1
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/xs_after_centrality_cuts
Title=$\sigma_\text{tot}$ after additional cut
LogY=0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP/xs_before_centrality_cuts
Title=$\sigma_\text{tot}$ before additional cut
LogY=0
# END PLOT
