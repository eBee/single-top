#include "MC_SINGLE_TOP.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Tools/SmearingFunctions.hh"
#include "Rivet/AnalysisLoader.hh"

namespace Rivet {

  /// Minimal constructor
  MC_SINGLE_TOP::MC_SINGLE_TOP() : Analysis("MC_SINGLE_TOP"),
  cuts{
    // N_jets = 2
    std::make_shared<MCSingleTopTwoJetsCut>(),
    // \Delta y_tj
    std::make_shared<MCSingleTopJetAbsRapidityCut>(0.5),
    std::make_shared<MCSingleTopJetAbsRapidityCut>(1.0),
    std::make_shared<MCSingleTopJetAbsRapidityCut>(1.5),
    std::make_shared<MCSingleTopJetAbsRapidityCut>(2.0),
    std::make_shared<MCSingleTopJetAbsRapidityCut>(2.5),
    std::make_shared<MCSingleTopJetAbsRapidityCut>(3.0),
    // G_T^(0)
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::zero, 1),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::zero, 5),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::zero, 10),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::zero, 20),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::zero, 30),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::zero, 40),
    // G_T^(t)
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::top, 1),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::top, 5),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::top, 10),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::top, 20),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::top, 30),
    std::make_shared<MCSingleGTCut>(GTObjectClass::jets, GTRapidityReferencePoint::top, 40),
    // g_T^(0)
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::zero, 1),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::zero, 5),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::zero, 10),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::zero, 20),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::zero, 30),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::zero, 40),
    // g_T^(t)
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::top, 1),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::top, 5),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::top, 10),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::top, 20),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::top, 30),
    std::make_shared<MCSingleGTCut>(GTObjectClass::tracks, GTRapidityReferencePoint::top, 40),
  },
  _h_j_mass(_njet),
  _h_j_pt(_njet),
  _h_j_y(_njet),
  _h_jt_mass(_njet),
  _h_jt_y(_njet),
  _h_t_y_minus_j_y(_njet)
  {
  }

  MC_SINGLE_TOP::MC_SINGLE_TOP(const std::string& name,
                               const std::vector<std::shared_ptr<MCSingleTopCut> > _cuts) :
  Analysis(name),
  cuts{ _cuts },
  _h_j_mass(_njet),
  _h_j_pt(_njet),
  _h_j_y(_njet),
  _h_jt_mass(_njet),
  _h_jt_y(_njet),
  _h_t_y_minus_j_y(_njet)
  {}

  void MC_SINGLE_TOP::init()
  {
    const double sqrts = sqrtS() ? sqrtS() : 8000.*GeV;

    // dressed electrons and/or muons (NOTE that we require tau leptons to be
    // either disabled in the event generation or properly decayed)
    const FinalState& photonfs{ Cuts::open() };
    const FinalState& leptonfs{ Cuts::abseta < 2.5 && Cuts::pT > 25*GeV };
    DressedLeptons leptons{ photonfs, leptonfs, 0.1 };
    addProjection(leptons, "DressedLeptons");

    VetoedFinalState jetfs{ FinalState{ Cuts::abseta < 4.5 && Cuts::pT > 0*GeV } };
    jetfs.addVetoOnThisFinalState(leptons);
    FastJets jets(jetfs, FastJets::ANTIKT, 0.4);
    addProjection(jets, "Jets");

    addProjection(MissingMomentum{}, "MissingET");

    addProjection(ChargedFinalState{ Cuts::abseta < 2.5 && Cuts::pT > 0.4*GeV }, "ChargedFinalState");

    _h_xs_before_centrality_cut = bookHisto1D("xs_before_centrality_cuts", 1, 0.0, 1.0);

    for (const auto& cut : cuts) {

      _h_l_pt[cut] = bookHisto1D("l_pt" + cut->histogramNameSuffix(), 40, 0, 200);
      _h_l_y[cut] = bookHisto1D("l_y" + cut->histogramNameSuffix(), 50, -5, 5);

      _h_W_mass[cut] = bookHisto1D("W_mass" + cut->histogramNameSuffix(), 60, 0, 300);
      _h_W_pt[cut] = bookHisto1D("W_pt" + cut->histogramNameSuffix(), 40, 0, 200);
      _h_W_y[cut] = bookHisto1D("W_y" + cut->histogramNameSuffix(), 50, -5, 5);

      _h_t_mass[cut] = bookHisto1D("t_mass" + cut->histogramNameSuffix(), 80, 0, 400);
      _h_t_pt[cut] = bookHisto1D("t_pt" + cut->histogramNameSuffix(), 40, 0, 200);
      _h_t_y[cut] = bookHisto1D("t_y" + cut->histogramNameSuffix(), 50, -5, 5);

      _h_b_mass[cut] = bookHisto1D("b_mass" + cut->histogramNameSuffix(), 50, 0, 100);
      _h_b_pt[cut] = bookHisto1D("b_pt" + cut->histogramNameSuffix(), 40, 0, 200);
      _h_b_y[cut] = bookHisto1D("b_y" + cut->histogramNameSuffix(), 50, -5, 5);

      for (size_t i = 0; i < _njet; ++i) {
        static const double mmax = 100.0;

        const string namesuffix{ to_str(i + 1) + cut->histogramNameSuffix() };
        const string massname = "j_mass_" + namesuffix;
        const int nbins_m = 100/(i+3);
        _h_j_mass[i][cut] = bookHisto1D(massname, logspace(nbins_m, 1.0, mmax));

        const double pTmax = 1.0/(double(i + 2)+2.0) * sqrts/GeV/2.0;
        const string pTname = "j_pt_" + namesuffix;
        const int nbins_pT = 100/(i+3);
        _h_j_pt[i][cut] = bookHisto1D(pTname, logspace(nbins_pT, 10.0, pTmax));

        const string rapname = "j_y_" + namesuffix;
        _h_j_y[i][cut] = bookHisto1D(rapname, 20, -5.0, 5.0);

        static const double jtmmax = 1000.0;
        const string jtmassname = "jt_mass_" + namesuffix;
        _h_jt_mass[i][cut] = bookHisto1D(jtmassname, logspace(nbins_m, 1.0, jtmmax));

        const string jtrapname = "jt_y_" + namesuffix;
        _h_jt_y[i][cut] = bookHisto1D(jtrapname, i>1 ? 25 : 50, -5.0, 5.0);

        const string jtdiffrapname = "t_y_minus_j_y_" + namesuffix;
        _h_t_y_minus_j_y[i][cut] = bookHisto1D(jtdiffrapname, i>1 ? 25 : 50, -5.0, 5.0);
      }

      _h_j_multi_exclusive[cut] = bookHisto1D("j_multi_exclusive" + cut->histogramNameSuffix(), _njet+26, -0.5, _njet+26-0.5);

      _h_xs_after_centrality_cut[cut] = bookHisto1D("xs_after_centrality_cuts" + cut->histogramNameSuffix(), 1, 0.0, 1.0);

    }
  }

  /// returns true if the GenParticle particle is from a hadronic decay
  /// excluding pure tau decays
  bool IsHadronicRemnant(const HepMC::GenParticle* p) { 
    if (p->production_vertex()->id() != 6) {
      return false;
    }
    // either hadronic or tau decay, for taus we will need to recurse
    bool processed_first{ false };
    for (HepMC::GenVertex::particles_in_const_iterator q{ p->production_vertex()->particles_in_const_begin() };
        q != p->production_vertex()->particles_in_const_end();
        ++q) {
      assert(!processed_first); // hadronic vertex is expected to have only one in-particle
      if (std::abs((*q)->pdg_id()) == 15) {
        return IsHadronicRemnant(*q);
      }
      processed_first = true;
    }
    return true;
  }

#define debug(literal) MSG_DEBUG(literal);

  void MC_SINGLE_TOP::analyze(const Event& event)
  {
    const MissingMomentum& met = apply<MissingMomentum>(event, "MissingET");
    if (met.missingPt() < 30*GeV)
      vetoEvent;

    const Particles charged_tracks{
      apply<ChargedFinalState>(event, "ChargedFinalState").particles() };

    // obtain photon-dressed electrons and muons
    const vector<DressedLepton>& dressedLeptons {
      apply<DressedLeptons>(event, "DressedLeptons").dressedLeptons() };
    debug(dressedLeptons.size() << " leptons dressed");
    if (dressedLeptons.size() == 0)
      vetoEvent;

    vector<DressedLepton> leptons;
    leptons.reserve(dressedLeptons.size());
    std::remove_copy_if(
        dressedLeptons.begin(), dressedLeptons.end(),
        std::back_inserter(leptons),
        [&](const DressedLepton& l) {
          return IsHadronicRemnant(l.constituentLepton().genParticle());
        });
    debug(leptons.size() << " leptons survive implicit W-boson match");

    if (leptons.size() != 1)
      vetoEvent;
    const auto& lepton = leptons[0];

    // obtain jets
    const FastJets& alljets = apply<FastJets>(event, "Jets");
    const Jets& jets = alljets.jetsByPt(30*GeV);

    for (const auto& jet : jets) {
      if (deltaR(jet.momentum(), lepton.momentum()) < 0.4)
        vetoEvent;
    }

    const double weight = event.weight();

    // sort jets into b-tagged jets and light jets
    Jets bjets, ljets;
    foreach (const Jet& jet, jets) {

      // an implementation taking into account b-tagging efficiencies and
      // mis-tagging acceptances, the numbers are taken from arXiv:1702.02859
      //const double r{ rand01() };
      //bool isbjet{ false };
      //if (jet.bTagged()) {
      //  ++true_bjet_multiplicity;
      //  isbjet = (r < 0.5);  // b-jet tagging efficiency
      //} else if (jet.cTagged()) {
      //  ++true_cjet_multiplicity;
      //  isbjet = (r < 0.039);  // c-jet mis-tagging acceptance
      //} else {
      //  ++true_ljet_multiplicity;
      //  isbjet = (r < 0.0007);  // light-jet mis-tagging accpetance
      //}
      //if (isbjet) {
      //  bjets.push_back(jet);
      //} else {
      //  ljets.push_back(jet);
      //}

      // we assume that b-tagging efficiencies and mis-tagging acceptances are
      // actually taken care of for the results in arXiv:1702.02859; hence, we
      // can use perfect experimental b-tagging here
      if (jet.bTagged() && jet.momentum().abseta() < 2.5) {
        bjets.push_back(jet);
      } else {
        ljets.push_back(jet);
      }
    }

    // require at least one light jet; in arXiv:1702.02859, exactly one light
    // jet is required, we implement this with an MCSingleTopCut subclass
    if (ljets.empty()) {
      debug(ljets.size() << " light jets -> veto");
      vetoEvent;
    }

    // b-jet cuts
    if (bjets.size() != 1) {
      debug(ljets.size() << " bottom jets -> veto");
      vetoEvent;
    }
    const Jet& bjet = bjets[0];
    if ((bjet.momentum() + lepton.momentum()).mass() > 160*GeV) {
      debug("too off-shell -> veto");
      vetoEvent;
    }

    debug("passed minimum cuts");

    // reconstruct neutrino momentum using W mass as constraint, cf.
    // arXiv:1502.05923
    const double a{ lepton.E2() - lepton.pz2() };
    static const double mW2{ 80.385*80.385 };
    const double k{ (mW2 - lepton.mass2()) / 2.0
      + ( lepton.px() * met.missingMomentum().px()
          + lepton.py() * met.missingMomentum().py() ) };
    const double b{ - 2.0 * k * lepton.pz() };
    const double c{ lepton.E2() * met.missingPt() * met.missingPt() - k * k};
    double neutrino_pz{ - b / (2.0 * a) };
    const double radicand{ b * b - 4.0 * a * c };
    if (radicand > 0.0) {
      const double reduced_sqrt{ sqrt(radicand) / (2.0 * a) };
      const double neutrino_pz1{ neutrino_pz + reduced_sqrt };
      const double neutrino_pz2{ neutrino_pz - reduced_sqrt };
      neutrino_pz = (std::abs(neutrino_pz1) < std::abs(neutrino_pz2)) ?
        neutrino_pz1 : neutrino_pz2;
    }
    FourMomentum neutrinomom;
    neutrinomom.setPM(met.missingMomentum().px(),
        met.missingMomentum().py(),
        neutrino_pz,
        0.0);

    // reconstruct W and top momentum
    const FourMomentum wmom{ lepton.momentum() + neutrinomom };
    const FourMomentum tmom{ wmom + bjet.momentum() };

    // filter charged tracks that are not within the b jet or "within" the lepton
    Particles remainingChargedTracks;
    for (const auto& track : charged_tracks) {
      if (std::abs(track.momentum().rapidity() - bjet.momentum().rapidity()) < 0.4) {
        continue;
      }
      if (std::abs(track.momentum().rapidity() - lepton.momentum().rapidity()) < 0.1) {
        continue;
      }
      remainingChargedTracks.push_back(track);
    }

    _h_xs_before_centrality_cut->fill(0.5, weight);

    for (const auto& cut : cuts) {

      if (cut->doesVeto(tmom, ljets, remainingChargedTracks)) continue;

      _h_xs_after_centrality_cut[cut]->fill(0.5, weight);

      // fill jetmulti-inclusive distributions
      _h_l_pt[cut]->fill(lepton.momentum().pt(), weight);
      _h_l_y[cut]->fill(lepton.momentum().rapidity(), weight);
      _h_W_mass[cut]->fill(wmom.mass(), weight);
      _h_W_pt[cut]->fill(wmom.pt(), weight);
      _h_W_y[cut]->fill(wmom.rapidity(), weight);
      _h_t_mass[cut]->fill(tmom.mass(), weight);
      _h_t_pt[cut]->fill(tmom.pt(), weight);
      _h_t_y[cut]->fill(tmom.rapidity(), weight);
      _h_b_mass[cut]->fill(bjet.momentum().mass(), weight);
      _h_b_pt[cut]->fill(bjet.momentum().pt(), weight);
      _h_b_y[cut]->fill(bjet.momentum().rapidity(), weight);
      _h_j_multi_exclusive[cut]->fill(ljets.size(), weight);

      // fill jetmulti-exclusive distributions
      for (size_t i = 0; i < _njet; ++i) {
        if (ljets.size() < i+1) continue;

        // jet pT
        _h_j_pt[i][cut]->fill(ljets[i].pT()/GeV, weight);

        // jet mass
        double m2_i = ljets[i].mass2();
        if (m2_i < 0) {
          if (m2_i < -1e-4) {
            MSG_WARNING("Jet mass2 is negative: " << m2_i << " GeV^2. "
                << "Truncating to 0.0, assuming numerical precision is to blame.");
          }
          m2_i = 0.0;
        }
        _h_j_mass[i][cut]->fill(sqrt(m2_i)/GeV, weight);

        // jet rapidity
        const double rap_i = ljets[i].rapidity();
        _h_j_y[i][cut]->fill(rap_i, weight);

        // multi-object quantities
        _h_jt_mass[i][cut]->fill((ljets[i] + tmom).mass(), weight);
        _h_jt_y[i][cut]->fill((ljets[i] + tmom).rapidity(), weight);
        _h_t_y_minus_j_y[i][cut]->fill(tmom.rapidity() - ljets[i].rapidity(), weight);
      }

    }

  }


  void MC_SINGLE_TOP::finalize()
  {
    const double sf{ crossSection() / sumOfWeights() };

    scale(_h_xs_before_centrality_cut, sf);

    for (const auto& cut : cuts) {
      scale(_h_l_pt[cut], sf);
      scale(_h_l_y[cut], sf);
      scale(_h_W_mass[cut], sf);
      scale(_h_W_pt[cut], sf);
      scale(_h_W_y[cut], sf);
      scale(_h_t_mass[cut], sf);
      scale(_h_t_pt[cut], sf);
      scale(_h_t_y[cut], sf);
      scale(_h_b_mass[cut], sf);
      scale(_h_b_pt[cut], sf);
      scale(_h_b_y[cut], sf);

      for (size_t i = 0; i < _njet; ++i) {
        scale(_h_j_mass[i][cut], sf);
        scale(_h_j_pt[i][cut], sf);
        scale(_h_j_y[i][cut], sf);
        scale(_h_jt_mass[i][cut], sf);
        scale(_h_jt_y[i][cut], sf);
        scale(_h_t_y_minus_j_y[i][cut], sf);
      }

      scale(_h_j_multi_exclusive[cut], sf);
      scale(_h_xs_after_centrality_cut[cut], sf);
    }
  }

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_SINGLE_TOP);

}

// vim: sw=2 et
