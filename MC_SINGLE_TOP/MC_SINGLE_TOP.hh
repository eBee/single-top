#ifndef RIVET_MC_SINGLE_TOP_HH
#define RIVET_MC_SINGLE_TOP_HH

#include "Rivet/Analysis.hh"

#include "utilities.hh"

#include <unordered_map>

namespace Rivet {

  typedef std::unordered_map<std::shared_ptr<MCSingleTopCut>, Histo1DPtr, MCSingleTopCutHash> CutHistoMap;
  typedef std::vector<CutHistoMap> CutHistoMapVector;

  class MC_SINGLE_TOP : public Analysis {
  public:

    MC_SINGLE_TOP();

    /// @name Analysis methods
    //@{

    void init();
    void analyze(const Event&);
    void finalize();

    //@}

  protected:

    // a constructor that does not set up cuts, such that subclasses can set up
    // their own
    MC_SINGLE_TOP(const std::string& name, const std::vector<std::shared_ptr<MCSingleTopCut> >);

    const std::vector<std::shared_ptr<MCSingleTopCut> > cuts;

  private:

    size_t _njet{ 4 };

    // @name Histogram data members
    //@{

    CutHistoMap _h_l_pt;
    CutHistoMap _h_l_y;

    CutHistoMap _h_W_mass;
    CutHistoMap _h_W_pt;
    CutHistoMap _h_W_y;

    CutHistoMap _h_t_mass;
    CutHistoMap _h_t_pt;
    CutHistoMap _h_t_y;

    CutHistoMap _h_b_mass;
    CutHistoMap _h_b_pt;
    CutHistoMap _h_b_y;

    CutHistoMapVector _h_j_mass;
    CutHistoMapVector _h_j_pt;
    CutHistoMapVector _h_j_y;

    CutHistoMapVector _h_jt_mass;
    CutHistoMapVector _h_jt_y;
    CutHistoMapVector _h_t_y_minus_j_y;

    CutHistoMap _h_j_multi_exclusive;

    Histo1DPtr _h_xs_before_centrality_cut;
    CutHistoMap _h_xs_after_centrality_cut;

    //@}

  };

}

#endif

// vim: sw=2 et
