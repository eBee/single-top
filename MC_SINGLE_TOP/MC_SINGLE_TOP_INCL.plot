# BEGIN PLOT /MC_SINGLE_TOP_INCL/Q
LogY=1
LogX=1
XLabel=$Q$
YLabel=$\mathrm{d}\sigma/\mathrm{d}Q$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/x
LogY=1
LogX=1
XLabel=$x_{1,2}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}x$ [pb]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/.*_y
XMin=-4
XMax=4
LogY=0
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/t_y
Title=$t$ rapidity
RatioPlotYMin=0.95
RatioPlotYMax=1.05
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_y_1
Title=$j_1$ rapidity
RatioPlotYMin=0.95
RatioPlotYMax=1.05
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_y_2
Title=$j_2$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_y_3
Title=$j_3$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_y_4
Title=$j_4$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_y_1
Title=$j_{b,1}$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_y_2
Title=$j_{b,2}$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_y_3
Title=$j_{b,3}$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_y_4
Title=$j_{b,4}$ rapidity
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/.*_pt
LogY=0
XLabel=$p_T$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_T$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_pt_
LogX=1
LogY=1
XLabel=$p_T$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_T$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_pt_
LogX=1
LogY=1
XLabel=$p_T$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_T$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_pt_1
Title=$j_1$ transverse momentum
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_pt_2
Title=$j_2$ transverse momentum
YMin=1e-12
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_pt_3
Title=$j_3$ transverse momentum
YMin=1e-15
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_pt_4
Title=$j_4$ transverse momentum
YMin=1e-15
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_pt_1
Title=$j_{b,1}$ transverse momentum
XMin=10
XMax=500
RatioPlotYMin=0.0
RatioPlotYMax=10.0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_pt_2
Title=$j_{b,2}$ transverse momentum
YMin=1e-12
YMax=1e0
XMin=10
XMax=500
RatioPlotYMin=0.0
RatioPlotYMax=25.0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_pt_3
Title=$j_{b,3}$ transverse momentum
YMin=1e-15
YMax=1e0
XMin=10
XMax=200
RatioPlotYMin=0.0
RatioPlotYMax=5.0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_pt_4
Title=$j_{b,4}$ transverse momentum
YMin=1e-15
YMax=1e0
XMin=10
XMax=100
RatioPlotYMin=0.0
RatioPlotYMax=5.0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/t_pt
Title=$t$ transverse momentum
LogY=1
RatioPlotYMin=0.2
RatioPlotYMax=5.0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/.*_mass
LogX=1
LogY=1
XLabel=$m$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}m$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_mass_1
Title=$j_1$ mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_mass_2
Title=$j_2$ mass
YMin=1e-12
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_mass_3
Title=$j_3$ mass
YMin=1e-15
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_mass_4
Title=$j_4$ mass
YMin=1e-18
YMax=1e0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_mass_1
Title=$j_{b,1}$ mass
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_mass_2
Title=$j_{b,2}$ mass
YMin=1e-12
YMax=1e0
XMin=10
XMax=80
RatioPlotYMin=0
RatioPlotYMax=15
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_mass_3
Title=$j_{b,3}$ mass
YMin=1e-15
YMax=1e0
XMin=10
XMax=60
RatioPlotYMin=0
RatioPlotYMax=15
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_mass_4
Title=$j_{b,4}$ mass
YMin=1e-18
YMax=1e0
XMin=10
XMax=20
RatioPlotYMin=0
RatioPlotYMax=15
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/t_mass
Title=$t$ mass
LogX=0
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/b_multi_exclusive
Title=exclusive bottom jet multiplicity
XMin=0
XMax=10
YMin=1e-12
YMax=1e1
YLabel=$\mathrm{d}\sigma/\mathrm{d}N_\mathrm{b-jets}$ [pb]
XLabel=$N_\mathrm{b-jets}$
XMin=-0.5
XMax=5.5
RatioPlotYMin=0
RatioPlotYMax=10
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_multi_exclusive
Title=exclusive light jet multiplicity
XMin=0
XMax=10
YMin=1e-12
YMax=1e1
YLabel=$\mathrm{d}\sigma/\mathrm{d}N_\mathrm{jets}$ [pb]
XLabel=$N_\mathrm{jets}$
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/j_multi_inclusive
Title=inclusive jet (light+bottom) multiplicity
XMin=0
XMax=10
YMin=1e-12
YMax=1e1
YLabel=$\mathrm{d}\sigma/\mathrm{d}N_\mathrm{b- and light jets}$ [pb]
XLabel=$N_\mathrm{b- and light jets}^\mathrm{incl}$
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_INCL/xs
Title=$\sigma_\text{tot}$
LogY=0
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT
