#include "MC_SINGLE_TOP_TWOJETS.hh"

#include "Rivet/AnalysisLoader.hh"

namespace Rivet {

  /// Minimal constructor
  MC_SINGLE_TOP_TWOJETS::MC_SINGLE_TOP_TWOJETS():
    MC_SINGLE_TOP("MC_SINGLE_TOP_TWOJETS", { std::make_shared<MCSingleTopTwoJetsCut>() })
  {
  }

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_SINGLE_TOP_TWOJETS);

}

// vim: sw=2 et
