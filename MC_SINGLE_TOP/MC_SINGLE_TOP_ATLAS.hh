#ifndef RIVET_MC_SINGLE_TOP_ATLAS_HH
#define RIVET_MC_SINGLE_TOP_ATLAS_HH

#include "Rivet/Analysis.hh"

#include "utilities.hh"

#include <unordered_map>

namespace Rivet {

  class MC_SINGLE_TOP_ATLAS : public Analysis {
  public:

    MC_SINGLE_TOP_ATLAS();

    /// @name Analysis methods
    //@{

    void init();
    void analyze(const Event&);
    void finalize();

    //@}

  private:

    // @name Histogram data members
    //@{

    Histo1DPtr _h_t_pt;
    Histo1DPtr _h_t_y;

    Histo1DPtr _h_j_pt;
    Histo1DPtr _h_j_y;

    Histo1DPtr _h_xs;

    //@}

  };

}

#endif

// vim: sw=2 et
