# BEGIN PLOT /MC_SINGLE_TOP_ATLAS/d05-x01-y01
Title=$j_1$ rapidity
LogY=0
XLabel=$|y|$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_ATLAS/d03-x01-y01
LogY=0
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_ATLAS/d04-x01-y01
XLabel=$j$ $p_T$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_T$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_ATLAS/d02-x01-y01
Title=$t$ transverse momentum
XLabel=$p_T$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_T$ [pb GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /MC_SINGLE_TOP_ATLAS/d01-x01-y01
LogY=0
YLabel=$\sigma$ [pb]
# END PLOT
