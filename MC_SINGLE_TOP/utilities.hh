#ifndef RIVET_MC_SINGLE_TOP_UTILITIES_HH
#define RIVET_MC_SINGLE_TOP_UTILITIES_HH

#include "Rivet/Jet.hh"

#include <sstream>

namespace Rivet {

  struct MCSingleTopCutHash;

  class MCSingleTopCut {

    public:
      virtual bool doesVeto(const FourMomentum& tmom,
                            const Jets& ljets,
                            const Particles& tracks) const = 0;
      std::string histogramNameSuffix() const;

    protected:
      MCSingleTopCut(const std::string& name = "");
      const std::string _name;

    private:
      size_t hash() const;
      friend bool operator==(const MCSingleTopCut&, const MCSingleTopCut&);
      friend MCSingleTopCutHash;

  };

  struct MCSingleTopCutHash {
    std::size_t operator()(const std::shared_ptr<MCSingleTopCut>&) const;
  };

  class MCSingleTopJetAbsRapidityCut : public MCSingleTopCut {
    
  public:
    MCSingleTopJetAbsRapidityCut(const double& absy_cut);
    bool doesVeto(const FourMomentum& tmom,
                  const Jets& ljets,
                  const Particles& tracks) const override;

  private:
    const double _absy_cut;

  };

  class MCSingleTopTwoJetsCut : public MCSingleTopCut {
    
  public:
    MCSingleTopTwoJetsCut();
    bool doesVeto(const FourMomentum& tmom,
                  const Jets& ljets,
                  const Particles& tracks) const override;

  };

  enum GTObjectClass : char { jets = 'G', tracks = 'g' } ;
  enum GTRapidityReferencePoint : char { zero = '0', top = 't' } ;
  template<typename T>
  std::string underlyingCharToString(T& t) { return std::string(1, static_cast<char>(t)); }

  class MCSingleGTCut : public MCSingleTopCut {
    
  public:
    MCSingleGTCut(const GTObjectClass&, const GTRapidityReferencePoint&, const double gt_cut);
    bool doesVeto(const FourMomentum& tmom,
                  const Jets& ljets,
                  const Particles& tracks) const override;

  private:
    const GTObjectClass object_class;
    const GTRapidityReferencePoint point;
    const double _gt_cut;
  };

}

#endif

// vim: sw=2 et
