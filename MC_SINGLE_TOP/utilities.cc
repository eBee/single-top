#include "utilities.hh"

namespace Rivet {

  std::string MCSingleTopCut::histogramNameSuffix() const
  {
    return "_" + _name;
  }

  MCSingleTopCut::MCSingleTopCut(const std::string& name): _name{ name } {}

  size_t MCSingleTopCut::hash() const {
    return std::hash<std::string>{}(_name);
  }

  bool operator==(const MCSingleTopCut& lhs, const MCSingleTopCut& rhs) {
    return lhs.hash() == rhs.hash();
  }

  std::size_t MCSingleTopCutHash::operator()(const std::shared_ptr<MCSingleTopCut>& c) const
  {
    return c->hash();
  }

  MCSingleTopJetAbsRapidityCut::MCSingleTopJetAbsRapidityCut(const double& absy_cut):
    MCSingleTopCut{ "absy_cut_" + to_str(absy_cut) },
    _absy_cut{ absy_cut }
  {}

  MCSingleTopTwoJetsCut::MCSingleTopTwoJetsCut():
    MCSingleTopCut{ "twojets_cut" }
  {}

  bool MCSingleTopJetAbsRapidityCut::doesVeto(const FourMomentum& tmom,
                                              const Jets& ljets,
                                              const Particles& tracks) const
  {
    for (const auto& ljet : ljets) {
      if (std::abs(ljet.momentum().rapidity() - tmom.rapidity()) < _absy_cut) {
        return true;
      }
    }
    return false;
  }

  bool MCSingleTopTwoJetsCut::doesVeto(const FourMomentum& tmom,
                                       const Jets& ljets,
                                       const Particles& tracks) const
  {
    // only allow for exactly one light jet
    return (ljets.size() != 1);
  }

  MCSingleGTCut::MCSingleGTCut(const GTObjectClass& c, const GTRapidityReferencePoint& p, const double gt_cut):
    MCSingleTopCut{ underlyingCharToString(c) + "T" + underlyingCharToString(p) + "_cut_" + to_str(gt_cut) },
    object_class{ c },
    point{ p },
    _gt_cut{ gt_cut }
  {}

  bool MCSingleGTCut::doesVeto(const FourMomentum& tmom,
                               const Jets& ljets,
                               const Particles& tracks) const
  {
    double gt{ 0.0 };
    std::vector<ParticleBase const*> jets_or_tracks;
    if (object_class == GTObjectClass::tracks) {
      for (auto& track : tracks)
        jets_or_tracks.push_back(&track);
    } else {
      for (auto& jet : ljets)
        jets_or_tracks.push_back(&jet);
    }
    for (const auto& jet_or_track : jets_or_tracks) {
      const FourMomentum& mom{ jet_or_track->momentum() };
      double rap{ mom.rapidity() };
      if (point == GTRapidityReferencePoint::top)
        rap -= tmom.rapidity();
      gt += mom.pT() * exp(-std::abs(rap));
    }
    return (gt > _gt_cut);
  }

}

// vim: sw=2 et
