#/usr/bin/env zsh

if (( $# != 2 )); then
  echo "usage: $0 source target"
  exit -1
fi

for ext in cc hh info; do
  cp $1.$ext $2.$ext
  sed -i "" "s/$1/$2/g" $2.$ext
done

# vim: sw=2 et
