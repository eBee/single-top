#include "MC_SINGLE_TOP_INCL.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Tools/SmearingFunctions.hh"
#include "Rivet/AnalysisLoader.hh"

namespace Rivet {

  /// Minimal constructor
  MC_SINGLE_TOP_INCL::MC_SINGLE_TOP_INCL() : Analysis("MC_SINGLE_TOP_INCL"),
  _h_j_mass(_njet),
  _h_j_pt(_njet),
  _h_j_y(_njet),
  _h_b_mass(_njet),
  _h_b_pt(_njet),
  _h_b_y(_njet),
  avgx(0.0),
  avgQ(0.0),
  totalweight(0.0)
  {
  }


  void MC_SINGLE_TOP_INCL::init()
  {
    const double sqrts = sqrtS() ? sqrtS() : 8000.*GeV;

    // dressed electrons and/or muons (NOTE that we require tau leptons to be
    // either disabled in the event generation or properly decayed)
    const FinalState& photonfs{ Cuts::open() };
    const FinalState& leptonfs{ Cuts::open() };
    DressedLeptons leptons{ photonfs, leptonfs, 0.1 };
    addProjection(leptons, "DressedLeptons");

    VetoedFinalState jetfs{ Cuts::open() };
    jetfs.addVetoOnThisFinalState(leptons);
    FastJets jets(jetfs, FastJets::ANTIKT, 0.4);
    addProjection(jets, "Jets");

    _h_xs = bookHisto1D("xs", 1, 0.0, 1.0);

    _h_x = bookHisto1D("x",  logspace(30, 1e-4, 1e0));
    _h_q = bookHisto1D("Q", logspace(30, 1e1, 1e3));

    _h_t_mass = bookHisto1D("t_mass", 80, 0, 400);
    _h_t_pt = bookHisto1D("t_pt", 40, 0, 200);
    _h_t_y = bookHisto1D("t_y", 50, -5, 5);

    for (size_t i = 0; i < _njet; ++i) {
      static const double mmax = 100.0;

      const string namesuffix{ to_str(i + 1) };
      const string massname = "_mass_" + namesuffix;
      const int nbins_m = 100/(i+3);
      _h_j_mass[i] = bookHisto1D("j" + massname, logspace(nbins_m, 1.0, mmax));
      _h_b_mass[i] = bookHisto1D("b" + massname, logspace(nbins_m, 1.0, mmax));

      const double pTmax = 1.0/(double(i + 2)+2.0) * sqrts/GeV/2.0;
      const string pTname = "_pt_" + namesuffix;
      const int nbins_pT = 100/(i+3);
      _h_j_pt[i] = bookHisto1D("j" + pTname, logspace(nbins_pT, 10.0, pTmax));
      _h_b_pt[i] = bookHisto1D("b" + pTname, logspace(nbins_pT, 10.0, pTmax));

      const string rapname = "_y_" + namesuffix;
      _h_j_y[i] = bookHisto1D("j" + rapname, i>1 ? 25 : 50, -5.0, 5.0);
      _h_b_y[i] = bookHisto1D("b" + rapname, i>1 ? 25 : 50, -5.0, 5.0);
    }

    _h_j_multi_exclusive = bookHisto1D("j_multi_exclusive", _maxjetmulti + 1, -0.5, _maxjetmulti + 0.5);
    _h_b_multi_exclusive = bookHisto1D("b_multi_exclusive", _maxjetmulti + 1, -0.5, _maxjetmulti + 0.5);
    _h_j_multi_inclusive = bookHisto1D("j_multi_inclusive", _maxjetmulti + 1, -0.5, _maxjetmulti + 0.5);
  }

  /// returns true if the GenParticle particle is a top quark
  bool IsTopQuark(const HepMC::GenParticle* p) { 
    return (std::abs(p->pdg_id()) == 6 && p->status() != 20);
  }

  void MC_SINGLE_TOP_INCL::analyze(const Event& event)
  {
    const double weight = event.weight();

    HepMC::PdfInfo const * pdfinfo{ event.genEvent()->pdf_info() };
    _h_x->fill(pdfinfo->x1(), weight);
    _h_x->fill(pdfinfo->x2(), weight);
    _h_q->fill(pdfinfo->scalePDF(), weight);

    avgx += pdfinfo->x1() * weight;
    avgx += pdfinfo->x2() * weight;
    avgQ += pdfinfo->scalePDF() * weight;
    totalweight += weight;

    // construct jets
    const FastJets& alljets = apply<FastJets>(event, "Jets");
    const Jets& jets = alljets.jetsByPt(30*GeV);

    // sort jets into b-tagged jets and light jets
    Jets bjets, ljets;
    foreach (const Jet& jet, jets) {
      if (jet.bTagged()) {
        bjets.push_back(jet);
      } else {
        ljets.push_back(jet);
      }
    }

    // (unphysically) extract top momentum to avoid b-jet assigment ambiguity
    Particles tops;
    for ( HepMC::GenEvent::particle_const_iterator p = event.genEvent()->particles_begin();
        p != event.genEvent()->particles_end(); ++p ) {
      if ( IsTopQuark(*p) ) {
        tops.emplace_back(*p);
      }
    }
    assert(tops.size() == 1);
    const FourMomentum tmom{ tops.front().momentum() };

    _h_xs->fill(0.5, weight);

    // fill top quark distributions
    _h_t_mass->fill(tmom.mass(), weight);
    _h_t_pt->fill(tmom.pt(), weight);
    _h_t_y->fill(tmom.rapidity(), weight);

    // fill jet multiplicities
    _h_j_multi_exclusive->fill(ljets.size(), weight);
    _h_b_multi_exclusive->fill(bjets.size(), weight);
    for (size_t i = 0; i <= _maxjetmulti; ++i) {
      if (jets.size() >= i) {
        _h_j_multi_inclusive->fill(i, weight);
      }
    }

    // fill jetmulti-exclusive distributions
    for (size_t i = 0; i < _njet; ++i) {
      if (ljets.size() < i+1) continue;

      // jet pT
      _h_j_pt[i]->fill(ljets[i].pT()/GeV, weight);

      // jet mass
      double m2_i = ljets[i].mass2();
      if (m2_i < 0) {
        if (m2_i < -1e-4) {
          MSG_WARNING("Jet mass2 is negative: " << m2_i << " GeV^2. "
              << "Truncating to 0.0, assuming numerical precision is to blame.");
        }
        m2_i = 0.0;
      }
      _h_j_mass[i]->fill(sqrt(m2_i)/GeV, weight);

      // jet rapidity
      const double rap_i = ljets[i].rapidity();
      _h_j_y[i]->fill(rap_i, weight);
    }
    for (size_t i = 0; i < _njet; ++i) {
      if (bjets.size() < i+1) continue;

      // jet pT
      _h_b_pt[i]->fill(bjets[i].pT()/GeV, weight);

      // jet mass
      double m2_i = bjets[i].mass2();
      if (m2_i < 0) {
        if (m2_i < -1e-4) {
          MSG_WARNING("b-jet mass2 is negative: " << m2_i << " GeV^2. "
              << "Truncating to 0.0, assuming numerical precision is to blame.");
        }
        m2_i = 0.0;
      }
      _h_b_mass[i]->fill(sqrt(m2_i)/GeV, weight);

      // jet rapidity
      const double rap_i = bjets[i].rapidity();
      _h_b_y[i]->fill(rap_i, weight);
    }
  }


  void MC_SINGLE_TOP_INCL::finalize()
  {
    const double sf{ crossSection() / sumOfWeights() };

    scale(_h_xs, sf);
    scale(_h_t_mass, sf);
    scale(_h_t_pt, sf);
    scale(_h_t_y, sf);
    scale(_h_x, sf);
    scale(_h_q, sf);

    for (size_t i = 0; i < _njet; ++i) {
      scale(_h_j_mass[i], sf);
      scale(_h_j_pt[i], sf);
      scale(_h_j_y[i], sf);
      scale(_h_b_mass[i], sf);
      scale(_h_b_pt[i], sf);
      scale(_h_b_y[i], sf);
    }

    scale(_h_j_multi_exclusive, sf);
    scale(_h_b_multi_exclusive, sf);
    scale(_h_j_multi_inclusive, sf);

    avgx /= totalweight * 2.0;
    avgQ /= totalweight;

    MSG_WARNING("avg x: " << avgx);
    MSG_WARNING("avg Q: " << avgQ);
  }

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_SINGLE_TOP_INCL);

}

// vim: sw=2 et
