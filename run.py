#!/usr/bin/env python
# coding=utf-8
"""
Walks through subdirectories of the current working directory
and start Sherpa processes for all run cards found.
All files with a '.dat' ending are assumed to be run cards.
Hidden directories (starting with '.') will be ignored.
"""

import os
import sys
import math
import argparse

try:
    import lhapdf
    HAS_LHAPDF = True
except ImportError:
    HAS_LHAPDF = False

# prevent cluttering our setup directory with pyc files
sys.dont_write_bytecode = True

import submitprocess

SCALE_VARIATIONS_TAG = 'SCALE_VARIATIONS'
PDF_VARIATIONS_TAG = 'PDF_VARIATIONS'


def main():
    """Entry point if this file is executed as a script."""

    # define and parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("target_path", default=".")
    parser.add_argument("-s", "--sherpa", default='Sherpa',
                        help='Sherpa binary')
    parser.add_argument("-c", "--run-command", default='run_local',
                        help="""Function for submitting jobs, must be defined
                        in submitprocess.py.""")
    parser.add_argument("-m", "--mode", default='all',
                        choices=["integration", "production", "all"])
    parser.add_argument("-a", "--analysis-prefix", default='',
                        help='Prefix for the analysis directory name')
    parser.add_argument("-r", "--pdf-members", default=None,
                        help="""A list of comma-delimited numbers or ranges to
                        overwrite the meaning of PDF[all].""")
    parser.add_argument("-n", "--random-seed-count", default=1, type=int,
                        help="""The number of random seeds to sample over. Only
                        used in production runs.""")
    parser.add_argument("-o", "--random-seed-offset", default=0, type=int,
                        help="""The first random seed is offset from 1 by this number.
                        Only used in production runs.""")
    parser.add_argument("-f", "--random-seed-file", default=None,
                        help="""A file that lists the random seeds to be used. Each
			line corresponds to one run.""")
    sampling_group = parser.add_mutually_exclusive_group()
    sampling_group.add_argument("-p", "--sampling-parameter", default=None,
                                help="""Tag values to sample over in
                                reweighting runs.  E.g. "TAG[0,1,2,3]" will
                                start reweighting runs with TAG:=0, TAG:=1, ...
                                passed to Sherpa with TAG-0, TAG-1, ... used as
                                a suffix for the analysis directory name. Only
                                used in production runs.""")
    sampling_group.add_argument("-P", "--sampling-parameter-all", default=None,
                                help="""Tag values to sample over in
                                reweighting AND dedicated runs.  E.g.
                                "TAG[0,1,2,3]" will start runs with TAG:=0,
                                TAG:=1, ... passed to Sherpa with TAG-0, TAG-1,
                                ... used as a suffix for the analysis directory
                                name. Only used in production runs.""")
    sampling_group.add_argument("-i", "--sampling-parameter-integration", default=None,
                                help="""Tag values to sample over in
                                reweighting runs. The difference to the -p
                                option is that also the integration is affected.""")
    parser.add_argument("-d", "--dedicated-runs-disabled", action="store_true")
    parser.add_argument("-v", "--variation-run-disabled", action="store_true")
    parser.add_argument("-t", "--force-single-run-resources", action="store_true")
    parser.add_argument("-y", "--dry-run", action="store_true")
    args = parser.parse_args()

    # ensure we have a prefix, because we intend to store results in a common
    # directory on scratch
    if args.analysis_prefix == '' and not args.mode == 'integration':
        raise Exception("Please specify an analysis prefix")

    # map run_command arg to run_command function
    if args.dry_run:
        run_command = getattr(submitprocess, 'run_dry')
    else:
        run_command = getattr(submitprocess, args.run_command)

    for run_card in get_run_cards(args.target_path):
        process_run_card(run_card, run_command, args)


def process_run_card(run_card, run_command, args):
    """Submits all processes required by mode for run_card based on run_command."""
    print "Processing run card: ", run_card

    # remember where we came from, then walk into MCEG working dir, picking up resources
    previous_dir = os.path.abspath(os.curdir)
    if not os.path.dirname(run_card) == "":
        os.chdir(os.path.dirname(run_card))
    resources = import_resources()

    if args.force_single_run_resources:
        resources["reweighting"] = resources["single"]

    # parse tag-sampling argument that is to be applied even to integrations
    overall_sampling_tag = None
    overall_sampling_values = [None]
    if args.sampling_parameter_integration is not None:
        overall_sampling_tag, overall_sampling_values = parse_sampling_argument(args.sampling_parameter_integration)

    for overall_sampling_value in overall_sampling_values:
        suffix = "-" + overall_sampling_value if overall_sampling_value is not None else ""

        working_dir_name = makedir_for_run_card(run_card, suffix)
        os.chdir(working_dir_name)

        # create common command parts
        sherpa_command  = [args.sherpa]
        sherpa_command += ['-f', '../' + os.path.basename(run_card)]
        if overall_sampling_value is not None:
            sherpa_command += ['{}:={}'.format(overall_sampling_tag, overall_sampling_value)]

        if args.mode in ('all', 'integration'):
            # integrate
            name = working_dir_name + '-prep'
            use_mpi = False
            try:
                if resources['integration']['nodes'] > 1:
                    use_mpi = True
            except KeyError:
                pass
            command = sherpa_command + ['-e0']
            if not use_mpi:
                command += ['; ./makelibs &&'] + sherpa_command + ['-e0']
            else:
                # TODO: check if Processes are available, if not use INIT_ONLY
                # overwrite number of nodes to 1 / disable MPI usage, i.e.  to
                # do the Process initialisation on a single core, then print
                # a warning that the script has to be called again
                pass
            run_command(command,
                        resources['integration'],
                        name=name,
                        logname='Integration')

        if args.mode in ('all', 'production'):

            sampling_tag = None
            reweighting_sampling_values = [None]
            dedicated_runs_sampling_values = [None]
            if args.sampling_parameter is not None:
                sampling_tag, reweighting_sampling_values = parse_sampling_argument(args.sampling_parameter)
            elif args.sampling_parameter_all is not None:
                sampling_tag, reweighting_sampling_values = parse_sampling_argument(args.sampling_parameter_all)
                dedicated_runs_sampling_values = reweighting_sampling_values

            random_seeds = []
            if args.random_seed_file is not None:
                with open(args.random_seed_file) as f:
                    for line in f:
                        random_seeds.append(line.strip())
            else:
                random_seeds = range(1 + args.random_seed_offset, args.random_seed_count + 1 + args.random_seed_offset)

            for random_seed in random_seeds:
                random_seed_args = ['-R', str(random_seed)]

                if args.variation_run_disabled:
                    print "No variation runs wanted ..."
                else:
                    for sampling_value in reweighting_sampling_values:

                        if sampling_value is not None:
                            sampling_args = [sampling_tag + ':=' + sampling_value]
                            # note that sampling_tag in the analysis path
                            # would be replaced by Sherpa's tag replacement
                            analysis_suffix = '-' + sampling_value
                        else:
                            sampling_args = []
                            analysis_suffix = ''

                        # run central (with on-the-fly variations)
                        name = working_dir_name + '-prod_rew-seed_' + str(random_seed)
                        analysis_output_args = get_analysis_output_sherpa_arg(args.analysis_prefix,
                                                                              random_seed,
                                                                              'Reweighting',
                                                                              analysis_suffix)
                        analysis_path = analysis_output_args[analysis_output_args.index('-A') + 1]
                        run_command(sherpa_command + random_seed_args
                                    + analysis_output_args + sampling_args,
                                    resources['reweighting'],
                                    name=name,
                                    logname=analysis_path)

                if args.dedicated_runs_disabled:
                    print "No dedicated runs wanted ..."
                else:
                    dedicated_sherpa_command = sherpa_command + random_seed_args
                    dedicated_sherpa_command += [SCALE_VARIATIONS_TAG + "=None", PDF_VARIATIONS_TAG + "=None"]

                    for sampling_value in dedicated_runs_sampling_values:

                        if sampling_value is not None:
                            sampling_args = [sampling_tag + ':=' + sampling_value]
                            # note that sampling_tag in the analysis path
                            # would be replaced by Sherpa's tag replacement
                            analysis_suffix = '-' + sampling_value
                        else:
                            sampling_args = []
                            analysis_suffix = ''

                        variations = parse_pdf_and_scale_variations('../' + os.path.basename(run_card), overwrite_pdfall=args.pdf_members)
                        print "Will iterate over variations:", variations

                        # run dedicated variations
                        for var in variations:
                            name = working_dir_name + '-ded'
                            name += '_scale_{}_{}_{}'.format(var[0], var[1], var[3])
                            if var[2] is not None:
                                name += '_{}_{}'.format(var[2][0], var[2][1])
                            name += '-seed_' + str(random_seed)
                            sherpa_args = dedicated_sherpa_command + sampling_args
                            sherpa_args += get_sherpa_args_for_variation(var,
                                                                        args.analysis_prefix,
                                                                        analysis_suffix,
                                                                        random_seed)
                            analysis_path = sherpa_args[sherpa_args.index('-A') + 1]
                            run_command(sherpa_args, resources['single'], name=name, logname=analysis_path)

        # reset state
        os.chdir(previous_dir)

def parse_sampling_argument(arg):
    """Return tag name and associated values parsing e.g. TAG[1,2,3]."""
    sampling_tag, rest = arg.split('[')
    sampling_values = [value.strip() for value in rest[:-1].split(',')]
    return sampling_tag, sampling_values

def get_sherpa_args_for_variation(variation, analysis_prefix, analysis_suffix, random_seed):
    """Returns arguments for the sherpa command for a given variation tuple."""
    rsf, fsf, pdf_and_version_or_none, shower_rsf = variation

    scale_factors_args = ['RSF:=' + str(rsf), 'FSF:=' + str(fsf), 'RENORMALIZATION_SCALE_FACTOR=' + str(shower_rsf)]

    base_name_pdf_part = ''
    pdf_args = []
    if pdf_and_version_or_none is not None:
        pdf, pdf_version = [str(arg) for arg in pdf_and_version_or_none]
        pdf_args = ['PDF_SET=' + pdf, 'PDF_SET_VERSION=' + pdf_version, 'ALPHAS_PDF_SET=' + pdf, 'ALPHAS_PDF_SET_VERSION=' + pdf_version]
        base_name_pdf_part = '.{}.{}'.format(pdf, pdf_version)

    base_name = 'Dedicated.MUR{}.MUF{}{}.PSMUR{}'.format(rsf, fsf, base_name_pdf_part, shower_rsf)
    analysis_output_args = get_analysis_output_sherpa_arg(analysis_prefix, random_seed, base_name, analysis_suffix)
    return scale_factors_args + pdf_args + analysis_output_args

def get_analysis_output_sherpa_arg(prefix, random_seed, base_name, suffix=''):
    """Combine the arguments to a canonical analysis output path and return the Sherpa args."""
    return ['-A', get_analysis_output_dir(prefix, random_seed, base_name, suffix=suffix) + base_name]

def get_analysis_output_dir(prefix, random_seed, base_name, suffix=''):
    """Combine the arguments to a canonical analysis output directory path."""
    return '/scratch/ebothma/single-top/' + prefix + '/Analysis/Seed.' + str(random_seed) + suffix + '/'

def get_run_cards(dir_or_file):
    """Returns all run cards at dir_or_file."""
    if os.path.isfile(dir_or_file):
        return [dir_or_file]
    found_run_cards = []
    for current_dir, sub_dirs, files in os.walk(dir_or_file):
        for file_name in files:
            basename, extension = os.path.splitext(file_name)
            if not extension == '.dat':
                continue
            # prune subdirectories with the same base name as a run card
            if basename in sub_dirs:
                sub_dirs.remove(basename)
            found_run_cards.append(os.path.join(current_dir, file_name))
        # prune all hidden subdirectories
        for sub_dir in list(sub_dirs):
            if sub_dir[0] == '.':
                sub_dirs.remove(sub_dir)

    return found_run_cards

def import_resources():
    """Returns the resources member from ./resources.py or None if this file does not exist."""
    sys.path.insert(0, os.path.abspath(os.curdir))
    try:
        from resources import resources
        return resources
    except ImportError:
        return {'integration': None, 'single': None, 'reweighting': None}

def makedir_for_run_card(run_card, suffix=""):
    """Makes sure a working dir for run_card exists and returns its name."""
    working_dir_name = basename_for_run_card(run_card) + suffix
    try:
        os.makedirs(working_dir_name)
    except OSError:
        pass
    return working_dir_name

def basename_for_run_card(run_card):
    """Return the base name of the path run_card assuming '.dat' as the extension."""
    return os.path.basename(run_card)[:-4]


def parse_pdf_and_scale_variations(file_name, overwrite_pdfall=None):
    """Returns all PDF and scale variations found in file_name in a common format.

    The format is as follows: [FSF, RSF, [PDF_SET, PDF_SET_VERSION], SHOWERRSF]."""
    variations = []
    pdf_variations = parse_pdf_variations(file_name, overwrite_pdfall=overwrite_pdfall)
    variations.extend([[1.0, 1.0, pdfvar, 1.0] for pdfvar in pdf_variations])
    variations.extend(parse_scale_variations(file_name))
    return variations

def parse_scale_variations(file_name):
    """Returns a [FSF, RSF, [PDF_SET, PDF_SET_VERSION], SHOWERRSF] list for
    each scale variation found in file_name.
    
    Note that the PDF tuple defaults to None if it's not specified in the run card,
    whereas an unspecified SHOWERRSF defaults to 1.0."""
    tuples = []
    variations = parse_parameters(file_name, SCALE_VARIATIONS_TAG)
    for variation in variations:
        args = [arg for arg in variation.strip(';').split(',')]
        ren_sf, fac_sf = [float(arg) for arg in args[:2]]
        try:
            shower_ren_sf = float(args[3])
        except IndexError:
            shower_ren_sf = 1.0
        try:
            pdfvars = parse_pdf_variations_argument(args[2])
            tuples.extend([[ren_sf, fac_sf, pdfvar, shower_ren_sf] for pdfvar in pdfvars])
        except IndexError:
            tuples.append([ren_sf, fac_sf, None, shower_ren_sf])
    return tuples

def parse_pdf_variations(file_name, overwrite_pdfall=None):
    """Returns a [PDF_SET, PDF_SET_VERSION] list for each PDF variation found in file_name."""
    tuples = []
    variations = parse_parameters(file_name, PDF_VARIATIONS_TAG)
    for variation in variations:
        tuples.extend(parse_pdf_variations_argument(variation, overwrite_pdfall=overwrite_pdfall))
    return tuples

def parse_pdf_variations_argument(arg, overwrite_pdfall=None):
    """Returns a [PDF_SET, PDF_SET_VERSION] list for each PDF variation found in arg."""
    all_pos = arg.find('[all]')
    if not all_pos == -1:
        pdf_set = arg[:all_pos]
        tuples = []
        if overwrite_pdfall is not None:
            ranges = overwrite_pdfall.split(',')
            for range_or_single_number in ranges:
                if '-' in range_or_single_number:
                    start, stop = range_or_single_number.split('-')
                else:
                    start = range_or_single_number
                    stop = range_or_single_number
                for repl in range(int(start), int(stop) + 1):
                    tuples.append([pdf_set, repl])
        else:
            for repl in range(1, get_number_of_versions(pdf_set)):
                tuples.append([pdf_set, repl])
        return tuples
    slash_pos = arg.find('/')
    if not slash_pos == -1:
        pdf_set = arg[:slash_pos]
        pdf_set_version = int(arg[slash_pos + 1:].strip(';'))
        return [[pdf_set, pdf_set_version]]
    raise Exception("Could not parse PDF arg: {}".format(arg))

def get_number_of_versions(pdf_set):
    """Returns the number of versions for pdf_set.
    If lhapdf is not available, the fallback value is 101."""
    if HAS_LHAPDF:
        pdf = lhapdf.mkPDF(pdf_set + '/0')
        return int(pdf.info().get_entry('NumMembers'))
    else:
        return 101

def parse_parameters(file_name, tag):
    """Returns parameters for a given run card tag."""
    with open(file_name) as run_card_contents:
        for line in run_card_contents:
            if line.split('#', 1)[0].strip().find(tag) == 0:
                return line.split()[1:]
    return []

if __name__ == "__main__":
    main()
