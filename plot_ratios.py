#!/usr/bin/env python2

import os
from string import Template
import matplotlib.pyplot as plt
import heppyplotlib as hpl
import heppyplotlib.configuration as hpc
import heppyplotlib.yodaplot as hpy
import heppyplotlib.rivetplot as hpr

try:
    os.mkdir('ratio-plots')
except OSError:
    pass

# why is this necessary? ~> bug in hpl
hpc.use_tex()

dirs = ['PDF-variation', 'PDF-variation-tbar']
dirs = [directory + '/Run/Analysis/' for directory in dirs]

files = ["Reweighting-0.yoda"]
for pdf in ["MMHT2014nlo68cl", "NNPDF30_nlo_as_0118", "abm11_5n_nlo"]:
    files.append("Dedicated.MUR1.0.MUF1.0.{}.0.PSMUR1.0.yoda".format(pdf))

rivet_paths = hpy.data_object_names(dirs[0] + files[0])
rivet_paths = [path for path in rivet_paths if not '/MC_XS/' in path]

html = ""

for rivet_path in rivet_paths:
    print("Processing {} ...".format(rivet_path))
    axes_list = None
    nominal_ratio = None
    for file_name in files:
        file_paths = [directory + file_name for directory in dirs]
        data_objects = []
        for file_path in file_paths:
            data_objects.append(hpy.resolve_data_object(file_path, rivet_path))
        histo_sum = data_objects[0] + data_objects[1]
        histo_diff = data_objects[0] - data_objects[1]
        ratio = histo_diff / histo_sum
        if nominal_ratio is None:
            nominal_ratio = ratio
        axes_list = hpl.ratioplot([ratio], '/', axes_list=axes_list, divide_by=nominal_ratio)[0]

    (main, diff) = axes_list

    plotinfo = hpr.load_plot_info(rivet_path)
    hpr.set_axis_scales(plotinfo, main, diff)

    main.set_yscale('linear')
    diff.set_ylim(0.5, 1.5)
    diff.set_yticks([0.5, 1.0, 1.5])
    diff.set_ylabel("ratio to CT14")
    main.set_ylabel("$(\\sigma_t - \\sigma_{\\bar{t}}) / (\\sigma_t + \\sigma_{\\bar{t}})$")

    observable = rivet_path.split('/')[-1]
    observable = observable.replace('_', '-')

    if observable == 'b-mass':
        main.set_ylim(0, 1)
        main.set_xlim(0, 40)

    plt.suptitle(observable + " asymmetry")

    (handles, labels) = main.get_legend_handles_labels()
    labels = ["CT14", "MMHT2014", "NNPDF3.0", "abm11"]
    main.legend(handles, labels)

    fig_filename = observable + '_ratio.png'
    plt.savefig('ratio-plots/' + fig_filename)

    html += "<img src='{}' alt='{}'><br>\n".format(fig_filename, observable)

with open('plot_ratios_index_template.html', 'r') as f:
    templatehtml = Template(f.read())
    with open('ratio-plots/index.html', 'w') as g:
        g.write(templatehtml.substitute(content=html))
