#!/usr/bin/env python2

import os
import sys
from string import Template
import matplotlib.pyplot as plt
import heppyplotlib as hpl
import heppyplotlib.configuration as hpc
import heppyplotlib.yodaplot as hpy
import heppyplotlib.rivetplot as hpr

channels = {}

analysis = "/MC_SINGLE_TOP_TWOJETS/"

def cut_label_twojets():
    return "$N_\\text{jets} = 2$"

def cut_label_GT(symbol='G', yref='0', cut=50):
    return "$" + symbol + "_{T, \\text{cut}}^{(" + yref + ")} = " + str(cut) + "\\,\\text{GeV}$"

def cut_label_Dy(cut=50):
    return "$y_\\text{cut} = " + str(cut) + "$"

def cut_suffix_twojets():
    return "_twojets_cut"

def cut_suffix_GT(symbol='G', yref='0', cut=50):
    return "_{}T{}_cut_{}".format(symbol, yref, cut)

def cut_suffix_Dy(cut=1.5):
    return "_absy_cut_{}".format(cut)

yodafiles = ["Dedicated.MUR1.0.MUF1.0.NNPDF30_nlo_as_0118.0.PSMUR1.0.yoda",
             "Dedicated.MUR1.0.MUF1.0.MMHT2014nlo68cl.0.PSMUR1.0.yoda",
             "Dedicated.MUR1.0.MUF1.0.CT14nlo.0.PSMUR1.0.yoda",
             "Dedicated.MUR1.0.MUF1.0.abm11_5n_nlo.0.PSMUR1.0.yoda"]
yodafiles = ["Run/Analysis/" + f for f in yodafiles]

yodafiles_bvar = ["MMHT", "CT14", "abm"]
yodafiles_bvar = ["Run.bvar-" + pdf + "/Analysis/Reweighting.yoda" for pdf in yodafiles_bvar]
yodafiles_bvar[:0] = [yodafiles[0]]

pdflabels = ["NNPDF30", "MMHT", "CT14", "abm11"]

channels["comparison"] = {
        "cuttext": "cuts",
        "cutsuffixes": [
            cut_suffix_twojets(),
            ],
        "labels": [
            cut_label_twojets(),
            ]
        }

unvaried_histograms = ["xs_before_centrality_cuts"]

for cname, cdata in channels.items():

    process = 'single-top-tchannel'
    texprocess = process
    diffylim = [0.5, 2.0]
    diffyticks = [0.5, 1.0, 1.5]
    process = "single-top production (t-channel)"
    texprocess = "$tj$ production (t-channel)"

    plots_path = 'plots-' + cname
    try:
        os.mkdir(plots_path)
    except OSError:
        pass

    # why is this necessary? ~> bug in hpl
    hpc.use_tex()

    yodafile = yodafiles[0]

    rivet_paths = hpy.data_object_names(yodafile)
    rivet_paths = [path for path in rivet_paths
                   if cdata["cutsuffixes"][0] in path]
    rivet_paths += unvaried_histograms
    rivet_paths.sort(key=str.lower)
    print(rivet_paths)

    headers = {'b_': 'bottom tag jet',
               'j_': '$p_T$-ordered light jets',
               'jt': 'top + light jet system',
               'l_': 'lepton',
               't_': 'reconstructed top quark',
               'W_': 'reconstructed $W$ boson',
               'xs': 'total cross sections'}

    html = ""

    ordered_keys = sorted(headers.keys())
    for anchor in ordered_keys:
        title = headers[anchor]
        html += "<a href=\"#{}\">{}</a><br>".format(anchor, title)

    for rivet_path in rivet_paths:
        histogram = rivet_path.split('/')[-1]
        histogram = histogram.replace(cdata["cutsuffixes"][0], "")
        base_rivet_path = analysis + histogram
        plotinfo = hpr.load_plot_info(base_rivet_path)
        try:
            key = histogram[:2]
            header = headers[key]
            html += "<h2 id=\"{}\">{}</h2>\n".format(key, header)
            del headers[key]
        except KeyError:
            pass
        print("Processing {} ...".format(histogram))
        rivet_path_variants = []
        if rivet_path in unvaried_histograms:
            rivet_path_variants.append(base_rivet_path)
        else:
            for suffix in cdata["cutsuffixes"]:
                rivet_path_variants.append(base_rivet_path + suffix)
        for curyodafiles, suffix in zip((yodafiles, yodafiles_bvar), ("", "_bvar")):
            data_objects = []
            for yodafile in curyodafiles:
                for rivet_path_variant in rivet_path_variants:
                    data_objects.append(hpy.resolve_data_object(yodafile, rivet_path_variant))
            axes_list = hpl.ratioplot(data_objects, base_rivet_path)[0]

            (main, diff) = axes_list

            hpr.set_axis_scales(plotinfo, main, diff)

            #main.set_yscale('linear')
            if not 'RatioPlotYMin' in plotinfo:
                diff.set_ylim(diffylim)
                diff.set_yticks(diffyticks)
            diff.set_ylabel("ratio to NNPDF")
            #main.set_ylabel("$\\text{d}\\sigma / \\text{d}
            #main.set_ylabel("$(\\sigma_t - \\sigma_{\\bar{t}}) / (\\sigma_t + \\sigma_{\\bar{t}})$")

            histogram = histogram.replace('_', '-')
            histogram_title = histogram.replace('-', ' ')

            #plt.suptitle(histogram)

            (handles, labels) = main.get_legend_handles_labels()
            main.legend(handles, pdflabels)

            fig_filename = histogram + suffix + '.png'
            fig_path = '{}/{}'.format(plots_path, fig_filename)
            plt.savefig(fig_path)
            plt.close()

            html += "<img src='{}' alt='{}'>\n".format(fig_filename, histogram + suffix)
        html += "<br>\n"

    with open('plot_index_template.html', 'r') as f:
        templatehtml = Template(f.read())
        with open(plots_path + '/index.html', 'w') as g:
            g.write(templatehtml.substitute(content=html, process=process, texprocess=texprocess, cuttext=cdata["cuttext"]))
