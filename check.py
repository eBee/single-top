#!/usr/bin/env python
# coding=utf-8
"""
Check if all YODA files found in sub directories of the passed Analysis
directory are actually there and whether they have the expected event count.

Examples:
    ./check.py 1-100 ,0,1,-1 500k
    ./check.py 1-100 500k
"""

import os
import sys

import yoda

from merge import get_files

check_strict_equality = True

def main():
    # define what is expected
    uses_tag_values = len(sys.argv) == 4
    seeds = parse_seed_argument(sys.argv[1])
    if uses_tag_values:
        tag_values = parse_tag_values_argument(sys.argv[2])
        event_count = parse_event_count_argument(sys.argv[3])
    else:
        tag_values = ['']
        event_count = parse_event_count_argument(sys.argv[2])
    event_count_tolerance = event_count / 1000  # per-mille deviations are okay
    print "Will test whether we deviate more than", event_count_tolerance, "from", event_count, "..."

    # expected yodafiles will be determined from the first directory being
    # inspected with a given tag
    expected_yodafiles = {}

    base_directories = ['Seed.' + str(seed) for seed in seeds]
    for base_directory in base_directories:
        for tag_value in tag_values:
            directory = base_directory if tag_value == '' else '{}-{}'.format(base_directory, tag_value)

            if not os.path.exists(directory):
                print "Missing directory:", directory
                continue

            yodafiles = set(get_files(directory, 'yoda'))

            if not tag_value in expected_yodafiles:
                expected_yodafiles[tag_value] = yodafiles
            else:
                if expected_yodafiles[tag_value] > yodafiles:
                    print "Missing files in " + directory + ":", expected_yodafiles[tag_value] - yodafiles
                elif check_strict_equality and not yodafiles == expected_yodafiles[tag_value]:
                    print "File content is not equal in " + directory + " compared with ", expected_yodafiles[tag_value]

            for yodafile in expected_yodafiles[tag_value]:
                filepath = os.path.join(directory, yodafile)
                try:
                    histos = yoda.readYODA(filepath)
                except Exception as e:
                    print "Could not read ", filepath
                try:
                    count = int(histos['/MC_XS/N'].numEntries())
                except Exception as e:
                    print "Could not read MC_XS/N in", filepath
                if abs(count - event_count) > event_count_tolerance:
                    print filepath, "has wrong statistics: ", count


def parse_seed_argument(arg):
    first, last = [int(i) for i in arg.split('-')]
    print "Will look for seeds from", str(first), "to", str(last), "..."
    return xrange(first, last + 1)


def parse_tag_values_argument(arg):
    tag_values = arg.split(',')
    print "Will look for the following tag values:", tag_values, "..."
    return tag_values


def parse_event_count_argument(arg):
    return int(arg[:-1]) * 1000


if __name__ == "__main__":
    main()
