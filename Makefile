sherpa_rel-2-2-3_mpi = /usr/users/ebothma/localscratch/sherpa-2.2.3/install/bin/Sherpa
sherpa_rel-2-2-3_nompi = /usr/users/ebothma/localscratch/sherpa-2.2.3/install-nompi/bin/Sherpa

sherpa_mpi = /usr/users/ebothma/localscratch/sherpa-2.2.4/install-mpi/bin/Sherpa
sherpa_nompi = /usr/users/ebothma/localscratch/sherpa-2.2.4/install-nompi/bin/Sherpa

disable_vars_args = PDF_VARIATIONS=None SCALE_VARIATIONS=None 

ct14_args = PDF_SET=CT14nlo
mmht2014_args = PDF_SET=MMHT2014nlo68cl
abm11_args = PDF_SET=abm11_5n_nlo
use_vary-b-only_args = PDF_LIBRARY=LHAPDFMultiSetSherpa

twojets_only_args = analyses:=MC_SINGLE_TOP_TWOJETS
nseeds = 500


### t t-channel 2.2.3-test ###

.PHONY: t-t_channel-2.2.3_test-preparation t-t_channel-2.2.3_test-integration t-t_channel-2.2.3_test-production

t-t_channel-2.2.3_test-preparation:
	./run.py t-t_channel-2.2.3_test/Run.dat \
		-a t-t_channel-2.2.3_test-preparation \
		-s "$(sherpa_rel-2-2-3_nompi) $(disable_vars_args) -e10k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-t_channel-2.2.3_test-integration:
	./run.py t-t_channel-2.2.3_test/Run.dat \
		-s $(sherpa_rel-2-2-3_mpi) \
		-m integration \
		-c run_scc

t-t_channel-2.2.3_test-production:
	./run.py t-t_channel-2.2.3_test/Run.dat \
		-a t-t_channel-2.2.3_test \
		-s "$(sherpa_rel-2-2-3_nompi)" \
		-m production \
		-c run_scc \
		-n 10 \
		-d

.PHONY: t-t_channel-2.2.3_test-Gmu_disabled-preparation t-t_channel-2.2.3_test-Gmu_disabled-integration t-t_channel-2.2.3_test-Gmu_disabled-production

t-t_channel-2.2.3_test-Gmu_disabled-preparation:
	./run.py t-t_channel-2.2.3_test-Gmu_disabled/Run.dat \
		-a t-t_channel-2.2.3_test-Gmu_disabled-preparation \
		-s "$(sherpa_rel-2-2-3_nompi) $(disable_vars_args) -e10k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-t_channel-2.2.3_test-Gmu_disabled-integration:
	./run.py t-t_channel-2.2.3_test-Gmu_disabled/Run.dat \
		-s $(sherpa_rel-2-2-3_mpi) \
		-m integration \
		-c run_scc

t-t_channel-2.2.3_test-Gmu_disabled-production:
	./run.py t-t_channel-2.2.3_test-Gmu_disabled/Run.dat \
		-a t-t_channel-2.2.3_test-Gmu_disabled \
		-s "$(sherpa_rel-2-2-3_nompi)" \
		-m production \
		-c run_scc \
		-n 10 \
		-d


.PHONY: t-t_channel-2.2.3_test-Gmu_disabled_in_MIG-preparation t-t_channel-2.2.3_test-Gmu_disabled_in_MIG-integration t-t_channel-2.2.3_test-Gmu_disabled_in_MIG-production

t-t_channel-2.2.3_test-Gmu_disabled_in_MIG-preparation:
	./run.py t-t_channel-2.2.3_test-Gmu_disabled_in_MIG/Run.dat \
		-a t-t_channel-2.2.3_test-Gmu_disabled_in_MIG-preparation \
		-s "$(sherpa_rel-2-2-3_nompi) $(disable_vars_args) -e10k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-t_channel-2.2.3_test-Gmu_disabled_in_MIG-integration:
	./run.py t-t_channel-2.2.3_test-Gmu_disabled_in_MIG/Run.dat \
		-s $(sherpa_rel-2-2-3_mpi) \
		-m integration \
		-c run_scc

t-t_channel-2.2.3_test-Gmu_disabled_in_MIG-production:
	./run.py t-t_channel-2.2.3_test-Gmu_disabled_in_MIG/Run.dat \
		-a t-t_channel-2.2.3_test-Gmu_disabled_in_MIG \
		-s "$(sherpa_rel-2-2-3_nompi)" \
		-m production \
		-c run_scc \
		-n 10 \
		-d

### t s-channel nf4 ###

.PHONY: t-s_channel-nf4-preparation t-s_channel-nf4-integration t-s_channel-nf4-production

t-s_channel-nf4-preparation:
	./run.py t-s_channel-nf4/Run.dat \
		-a t-s_channel-nf4-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-s_channel-nf4-integration:
	./run.py t-s_channel-nf4/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

t-s_channel-nf4-production:
	# run central value and on-the-fly variations
	./run.py t-s_channel-nf4/Run.dat \
		-a t-s_channel-nf4 \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d

### tbar s-channel nf4 ###

.PHONY: tbar-s_channel-nf4-preparation tbar-s_channel-nf4-integration tbar-s_channel-nf4-production

tbar-s_channel-nf4-preparation:
	./run.py tbar-s_channel-nf4/Run.dat \
		-a tbar-s_channel-nf4-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

tbar-s_channel-nf4-integration:
	./run.py tbar-s_channel-nf4/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

tbar-s_channel-nf4-production:
	# run central value and on-the-fly variations
	./run.py tbar-s_channel-nf4/Run.dat \
		-a tbar-s_channel-nf4 \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### t t-channel nf4 ###

.PHONY: t-t_channel-nf4-preparation t-t_channel-nf4-integration t-t_channel-nf4-production

t-t_channel-nf4-preparation:
	./run.py t-t_channel-nf4/Run.dat \
		-a t-t_channel-nf4-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-t_channel-nf4-integration:
	./run.py t-t_channel-nf4/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

t-t_channel-nf4-production:
	# run central value and on-the-fly variations
	./run.py t-t_channel-nf4/Run.dat \
		-a t-t_channel-nf4-distributions \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### tbar t-channel nf4 ###

.PHONY: tbar-t_channel-nf4-preparation tbar-t_channel-nf4-integration tbar-t_channel-nf4-production

tbar-t_channel-nf4-preparation:
	./run.py tbar-t_channel-nf4/Run.dat \
		-a tbar-t_channel-nf4-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

tbar-t_channel-nf4-integration:
	./run.py tbar-t_channel-nf4/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

tbar-t_channel-nf4-production:
	# run central value and on-the-fly variations
	./run.py tbar-t_channel-nf4/Run.dat \
		-a tbar-t_channel-nf4-distributions \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### t t-channel nf4 var scale ###

.PHONY: t-t_channel-nf4-var_scale-preparation t-t_channel-nf4-var_scale-integration t-t_channel-nf4-var_scale-production

t-t_channel-nf4-var_scale-preparation:
	./run.py t-t_channel-nf4-var_scale/Run.dat \
		-a t-t_channel-nf4-var_scale-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-t_channel-nf4-var_scale-integration:
	./run.py t-t_channel-nf4-var_scale/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

t-t_channel-nf4-var_scale-nlo-integration:
	./run.py t-t_channel-nf4-var_scale-nlo/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

t-t_channel-nf4-bottom_scale-integration:
	./run.py t-t_channel-nf4-bottom_scale/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

t-t_channel-nf4-var_scale-production:
	# run central value and on-the-fly variations
	./run.py t-t_channel-nf4-var_scale/Run.dat \
		-a t-t_channel-nf4-var_scale \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### t t-channel ###

.PHONY: t-t_channel-preparation t-t_channel-integration t-t_channel-production

t-t_channel-preparation:
	./run.py t-t_channel/Run.dat \
		-a t-t_channel-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-t_channel-integration:
	./run.py t-t_channel/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

t-t_channel-production:
	# run central value and on-the-fly variations
	./run.py t-t_channel/Run.dat \
		-a t-t_channel \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d
	# explicit PDF variations (do not need full analyses here)
	./run.py t-t_channel/Run.dat \
		-a t-t_channel-PDFset_variation_all-CT14 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(ct14_args)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py t-t_channel/Run.dat \
		-a t-t_channel-PDFset_variation_all-MMHT2014 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(mmht2014_args)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py t-t_channel/Run.dat \
		-a t-t_channel-PDFset_variation_all-ABM11 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(abm11_args)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	# explicit b-only PDF variations (do not need full analyses here)
	./run.py t-t_channel/Run.dat \
		-a t-t_channel-PDFset_variation_b_only-CT14 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(use_vary-b-only_args) PDF_SET_BY_ID=5,CT14nlo,-5,CT14nlo MI_RESULT_DIRECTORY_SUFFIX=-CT14-b_only" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py t-t_channel/Run.dat \
		-a t-t_channel-PDFset_variation_b_only-MMHT2014 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(use_vary-b-only_args) PDF_SET_BY_ID=5,MMHT2014nlo68cl,-5,MMHT2014nlo68cl MI_RESULT_DIRECTORY_SUFFIX=-MMHT2014-b_only" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py t-t_channel/Run.dat \
		-a t-t_channel-PDFset_variation_b_only-abm \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(use_vary-b-only_args) PDF_SET_BY_ID=5,abm11_5n_nlo,-5,abm11_5n_nlo MI_RESULT_DIRECTORY_SUFFIX=-abm11-b_only" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d

t-t_channel-distributions-production:
	# run central value and on-the-fly variations
	./run.py t-t_channel/Run-ATLAS.dat \
		-a t-t_channel-distributions \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### anti-t t-channel ###

.PHONY: tbar-t_channel-preparation tbar-t_channel-integration tbar-t_channel-production

tbar-t_channel-preparation:
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

tbar-t_channel-integration:
	./run.py tbar-t_channel/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

tbar-t_channel-production:
	# run central value and on-the-fly variations
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d
	# explicit PDF variations (do not need full analyses here)
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel-PDFset_variation_all-CT14 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(ct14_args)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel-PDFset_variation_all-MMHT2014 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(mmht2014_args)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel-PDFset_variation_all-ABM11 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(abm11_args)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	# explicit b-only PDF variations (do not need full analyses here)
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel-PDFset_variation_b_only-CT14 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(use_vary-b-only_args) PDF_SET_BY_ID=5,CT14nlo,-5,CT14nlo MI_RESULT_DIRECTORY_SUFFIX=-CT14-b_only" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel-PDFset_variation_b_only-MMHT2014 \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(use_vary-b-only_args) PDF_SET_BY_ID=5,MMHT2014nlo68cl,-5,MMHT2014nlo68cl MI_RESULT_DIRECTORY_SUFFIX=-MMHT2014-b_only" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d
	./run.py tbar-t_channel/Run.dat \
		-a tbar-t_channel-PDFset_variation_b_only-abm \
		-s "$(sherpa_nompi) $(disable_vars_args) $(twojets_only_args) $(use_vary-b-only_args) PDF_SET_BY_ID=5,abm11_5n_nlo,-5,abm11_5n_nlo MI_RESULT_DIRECTORY_SUFFIX=-abm11-b_only" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-t \
		-d


### t s-channel ###

.PHONY: t-s_channel-preparation t-s_channel-integration t-s_channel-production

t-s_channel-preparation:
	./run.py t-s_channel/Run.dat \
		-a t-s_channel-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

t-s_channel-integration:
	./run.py t-s_channel/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

t-s_channel-production:
	# run central value and on-the-fly variations
	./run.py t-s_channel/Run.dat \
		-a t-s_channel \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### anti-t s-channel ###

.PHONY: tbar-s_channel-preparation tbar-s_channel-integration tbar-s_channel-production

tbar-s_channel-preparation:
	./run.py tbar-s_channel/Run.dat \
		-a tbar-s_channel-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

tbar-s_channel-integration:
	./run.py tbar-s_channel/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

tbar-s_channel-production:
	# run central value and on-the-fly variations
	./run.py tbar-s_channel/Run.dat \
		-a tbar-s_channel \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### top pair production ###

.PHONY: ttbar-preparation ttbar-integration ttbar-production

ttbar-preparation:
	./run.py ttbar/Run.dat \
		-a ttbar-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

ttbar-integration:
	./run.py ttbar/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

ttbar-production:
	# run central value and on-the-fly variations
	./run.py ttbar/Run.dat \
		-a ttbar \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d


### w plus production ###

.PHONY: wplus-preparation wplus-integration wplus-production

wplus-preparation:
	./run.py wplus/Run.dat \
		-a wplus-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

wplus-integration:
	./run.py wplus/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

wplus-production:
	# run central value and on-the-fly variations
	./run.py wplus/Run.dat \
		-a wplus \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-o 500 \
		-d


### w minus production ###

.PHONY: wminus-preparation wminus-integration wminus-production

wminus-preparation:
	./run.py wminus/Run.dat \
		-a wminus-preparation \
		-s "$(sherpa_nompi) $(disable_vars_args) -e1k" \
		-m production \
		-c run_scc \
		-n 1 \
		-t \
		-d

wminus-integration:
	./run.py wminus/Run.dat \
		-s $(sherpa_mpi) \
		-m integration \
		-c run_scc

wminus-production:
	# run central value and on-the-fly variations
	./run.py wminus/Run.dat \
		-a wminus \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d

### tW production ###

tw-had-production:
	# run central value and on-the-fly variations
	./run.py tW/Run.had.dat \
		-a tw-had \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d

### tW-nlo production ###

tw-nlo-integration:
	# run central value and on-the-fly variations
	./run.py tW-nlo/Run.dat \
		-s "$(sherpa_mpi)" \
		-m integration \
		-c run_scc

tw-nlo-production:
	# run central value and on-the-fly variations
	./run.py tW-nlo/Run.dat \
		-a tw-nlo \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d

tbarw-nlo-integration:
	# run central value and on-the-fly variations
	./run.py tbarW-nlo/Run.dat \
		-s "$(sherpa_mpi)" \
		-m integration \
		-c run_scc

tbarw-nlo-production:
	# run central value and on-the-fly variations
	./run.py tbarW-nlo/Run.dat \
		-a tbarw-nlo \
		-s "$(sherpa_nompi)" \
		-m production \
		-c run_scc \
		-n $(nseeds) \
		-d
