"""
Definition of the resources used for runs in this directory.
Walltime in minutes, mem[ory] in GB.
"""
resources = {}
resources['single']      = {'walltime':  6*60, 'mem': 2}
resources['integration'] = {'walltime':    60, 'mem': 2, 'nodes': 32}
resources['reweighting'] = {'walltime': 48*60, 'mem': 2}
